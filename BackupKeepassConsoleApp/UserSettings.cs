﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace BackupKeepassConsoleApp
{
    public static class UserSettings
    {
        public static string getPassword()
        {


            if (string.IsNullOrEmpty(Properties.Settings.Default.password))
            {
                Console.Write("Please Enter Your SFTP Password:");
                ConsoleKeyInfo key;
                var pwd = new SecureString();
                while (true)
                {
                    ConsoleKeyInfo i = Console.ReadKey(true);
                    if (i.Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                    else if (i.Key == ConsoleKey.Backspace)
                    {
                        if (pwd.Length > 0)
                        {
                            pwd.RemoveAt(pwd.Length - 1);
                            Console.Write("\b \b");
                        }
                    }
                    else
                    {
                        pwd.AppendChar(i.KeyChar);
                        Console.Write("*");
                    }
                }

                Console.WriteLine();

                string pass = Encryption.EncryptString(pwd);
                Properties.Settings.Default.password = pass;
                Properties.Settings.Default.Save();
            }
            return Properties.Settings.Default.password;
        }
        public static string getUsername()
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.username))
            {
                Console.Write("Please Enter Your SFTP Username:");
                string uname = Console.ReadLine();
                Properties.Settings.Default.username = uname;
                Properties.Settings.Default.Save();
            }
            return Properties.Settings.Default.username;
        }
        public static string getHostname()
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.hostname))
            {
                Console.Write("Please Enter Your SFTP Hostname:");
                string hname = Console.ReadLine();
                Properties.Settings.Default.hostname = hname;
                Properties.Settings.Default.Save();
            }
            return Properties.Settings.Default.hostname;
        }
    }
}
