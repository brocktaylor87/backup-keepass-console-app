﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupKeepassConsoleApp
{
    public static class Backup
    {

        internal static void CompleteOperation(string hostname, string username, string password)
        {
            
            var day = DateTime.Now.Day;

            MoveBackups();

            //If it's the 1st, 7th, 14th, 21st or 28th of Month, download a copy to the specified folder.
            if (day == 1 || day == 7 || day == 14 || day == 21 || day == 28)
            {
                Directory.CreateDirectory("C:\\Backups\\KeePass" + "\\" + day.ToString());
                Download.DownloadFile(hostname, username, Encryption.DecryptString(password), "kpdb/mykpdb.kdbx", "C:\\Backups\\KeePass" + "\\" + day.ToString() + "\\mykpdb.kdbx");
            }

            Directory.CreateDirectory("C:\\Backups\\KeePass\\Daily");
            //Download a copy to the "Daily" folder.
            Download.DownloadFile(hostname, username, Encryption.DecryptString(password), "kpdb/mykpdb.kdbx", "C:\\Backups\\KeePass\\Daily\\mykpdb.kdbx");
        }

        internal static void MoveBackups()
        {
            Directory.CreateDirectory("C:\\Backups\\KeePass\\Yesterday");
            for (var i = 2; i < 5; i++)
            {
                Directory.CreateDirectory("C:\\Backups\\KeePass\\" + i.ToString() + "DaysAgo");
            }
            for (var i = 3; i > -1; i--)
            {
                if (i == 0)
                {
                    if (File.Exists("C:\\Backups\\KeePass\\Daily\\mykpdb.kdbx"))
                    {
                        File.Copy("C:\\Backups\\KeePass\\Daily\\mykpdb.kdbx", "C:\\Backups\\KeePass\\Yesterday\\mykpdb.kdbx", true);
                    }
                }
                else if (i == 1)
                {
                    if (File.Exists("C:\\Backups\\KeePass\\Yesterday\\mykpdb.kdbx"))
                    {
                        File.Copy("C:\\Backups\\KeePass\\Yesterday\\mykpdb.kdbx", "C:\\Backups\\KeePass\\2DaysAgo\\mykpdb.kdbx", true);
                    }
                }
                else if (File.Exists("C:\\Backups\\KeePass\\" + i.ToString() + "DaysAgo\\mykpdb.kdbx"))
                {
                    File.Copy("C:\\Backups\\KeePass\\" + i.ToString() + "DaysAgo\\mykpdb.kdbx", "C:\\Backups\\KeePass\\" + (i + 1).ToString() + "DaysAgo\\mykpdb.kdbx", true);
                }
            }
        }
    }
}
