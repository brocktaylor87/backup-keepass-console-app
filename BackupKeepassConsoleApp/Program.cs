﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupKeepassConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Properties.Settings.Default.Reset();
                var hostname = UserSettings.getHostname();
                var username = UserSettings.getUsername();
                var password = UserSettings.getPassword();

                Backup.CompleteOperation(hostname, username, password);
                
                Email.SendEmail("brock@brockstaylor.com", "Keepass Backup Automailer", "Automailer@brockstaylor.com", "SUCCESS: Keepass Backup " + DateTime.Now.ToString(), "Keepass Database backed up successfully to C:\\Backups\\KeePass\\mykpdb.kdbx <br /><br />Timestamp: " + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                Email.SendEmail("brock@brockstaylor.com", "Keepass Backup Automailer", "Automailer@brockstaylor.com", "FAILED: Keepass Backup " + DateTime.Now.ToString(), "Keepass backup failed with the following exception: <br /><br />" + ex.ToString());
            }
        }
    }
}
