﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BackupKeepassConsoleApp
{
    public static class Download
    {
        public static void DownloadFile(string hostname, string userName, System.Security.SecureString password, string ftpSourceFilePath, string localDestinationFilePath)
        {
            SftpDownload(hostname, userName, password, ftpSourceFilePath, localDestinationFilePath);
        }

        private static void SftpDownload(string hostname, string username, System.Security.SecureString password, string remoteFileName, string localFileName)
        {
            using (var sftp = new SftpClient(hostname, username, Encryption.ToInsecureString(password)))
            {
                sftp.Connect();
                using (var file = File.OpenWrite(localFileName))
                {
                    sftp.DownloadFile(remoteFileName, file);
                }
                sftp.Disconnect();
            }
        }
    }
}
