﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupKeepassConsoleApp
{
    public static class Email
    {

        public static IRestResponse SendEmail(string to, string fromName, string fromAddress, string subject, string message)
        {
            RestClient client = new RestClient();
            client.BaseUrl = "https://api.mailgun.net/v2";
            client.Authenticator = new HttpBasicAuthenticator("api","key-0ya87uf65svmlwt6895ftyfrqt7kv5o5");
            RestRequest request = new RestRequest();
            request.AddParameter("domain","brockstaylor.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", fromName + "<" + fromAddress + ">");
            request.AddParameter("to", to);
            request.AddParameter("subject", subject);
            request.AddParameter("html", message);
            request.Method = Method.POST;
            return client.Execute(request);
        }
    }
}
